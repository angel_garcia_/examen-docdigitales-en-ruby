class CreateEmpleados < ActiveRecord::Migration[5.1]
  def change
    create_table :empleados do |t|
      t.string :nombre
      t.string :puesto
      t.references :sucursal, foreign_key: true
      t.string :rfc

      t.timestamps
    end
  end
end
