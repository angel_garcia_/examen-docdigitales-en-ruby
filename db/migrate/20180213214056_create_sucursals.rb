class CreateSucursals < ActiveRecord::Migration[5.1]
  def change
    create_table :sucursals do |t|
      t.string :nombre
      t.string :calle
      t.string :colonia
      t.string :numExt
      t.string :numInt
      t.integer :cp
      t.string :ciudad
      t.string :pais
      t.integer :status
      t.references :user, foreign_key: true

      t.timestamps null: false
    end
  end
end
