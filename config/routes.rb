Rails.application.routes.draw do

  # get 'empleados/index'

  # get 'empleados/new'

  # get 'empleados/show'

  # get 'empleados/update'

  # get 'empleados/destroy'

  # get 'sucursales/index'
  # get 'sucursales/'

  # get 'sucursales/new'

  # get 'home/index'

  devise_for :users
	root to: "sucursales#index"
  resources :sucursales
	resources :empleados
	# resources :home
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
