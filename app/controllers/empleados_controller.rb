class EmpleadosController < ApplicationController
  before_action :authenticate_user!
  def index
  end

  def new
    @empleado = Empleado.new
    @sucursalesDisp = Sucursal.where(user_id: current_user.id)

    
  end

  def edit
      @empleado       = Empleado.find(params[:id])
      @suc            = Sucursal.where('id': @empleado.sucursal_id)
      @sucursalesDisp = Sucursal.where(user_id: current_user.id)
    end

  def show
  end

  def create
    @empleado = Empleado.new
    @sucursalesDisp = Sucursal.where(user_id: current_user.id)
    # Se revisa si hay cupo para el nuevo empleado en la sucursal
    if  !params[:empleado][:sucursal_id].present?
      @error = "No se selecciono ninguna sucursal";
      redirect_to new_empleado_path, notice: "No se selecciono ninguna sucursal"
    else
      @cupo = Empleado.where('sucursal_id': params[:empleado][:sucursal_id].to_i).count
      if @cupo >= 5 
        flash[:notice] = "La sucursal seleccionada ya tiene 5 empleados"
        redirect_to new_empleado_path
      else
        @empleado = Empleado.new(parametros) 
        if @empleado.save
          redirect_to sucursale_path(@empleado.sucursal_id)
        else
          render 'new'
        end
      end
    end
  end

  def update
      @empleado = Empleado.find(params[:id])
      # byebug
      # Se esta actualizando informacion de un empleado existente en la sucursal
      if @empleado.sucursal_id == params[:empleado][:sucursal_id].to_i
        if @empleado.update(parametros)
          redirect_to sucursale_path(@empleado.sucursal_id)
        else
          @sucursalesDisp = Sucursal.where(user_id: current_user.id)
          @suc            = Sucursal.where('id': @empleado.sucursal_id)
          render 'edit'
        end
      else
        # Se esta actualizando un empleado desde otra sucursal, hay que validar
        @cambio = Empleado.where('sucursal_id': params[:empleado][:sucursal_id].to_i).count
        if @cambio >= 5 
          @suc            = Sucursal.where('id': @empleado.sucursal_id.to_i)
          @sucursalesDisp = Sucursal.where(user_id: current_user.id)
          @error = "La sucursal seleccionada ya tiene 5 empleados"
          render 'edit'
          # byebug
        else
          if @empleado.update(parametros)
            # byebug
            redirect_to sucursale_path(@empleado.sucursal_id)
          else
            render 'edit'
          end
        end
      end
    end

  def destroy
      @empleado = @sucursal = Empleado.find(params[:id])
      @empleado.destroy
      redirect_to sucursale_path(@sucursal.sucursal_id)
  end

  private

    def parametros
      params.require(:empleado).permit(:nombre,:rfc,:puesto,:sucursal_id)
      
    end


end
