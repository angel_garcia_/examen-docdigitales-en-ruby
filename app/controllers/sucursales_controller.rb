class SucursalesController < ApplicationController
	before_action :authenticate_user!
  	def index
  		
  		@sucursal =  Sucursal.paginate(:page => params[:page], :per_page => 10).where(user_id:current_user.id)

  	end

	def new
	  	@sucursal = Sucursal.new
	  	@sucursal.empleados.new
	  	# render 'new'
	end
	
	def create
		@sucursal = current_user.sucursal.new(parametros) 
		#byebug
		if @sucursal.save
			# Empleado.new(params[:empleados_attributes])
			redirect_to sucursales_path
		else
			render "new"
			
		end
	end
	
	def show
	  	@sucursal =  Sucursal.find(params[:id]);	
	  	@empleados = Empleado.buscar_por_sucursal(params[:id])
      # byebug
  	end
	
	def edit
	  	@sucursal =  Sucursal.find(params[:id]);
  	end

  	def update
  		@sucursal = Sucursal.find(params[:id])

  		if @sucursal.update(parametros)
  			redirect_to sucursale_path
  		else
  			render 'edit'
  		end
  		
  	end

  	def destroy
  		Sucursal.find(params[:id]).destroy
  		redirect_to sucursales_path
  	end

  private

  	def parametros
  		params.require(:sucursal).permit(:nombre, :colonia, :calle, :cp, :pais, :ciudad, :numInt, :numExt, empleados_attributes:[:nombre,:rfc,:puesto,:_destroy])
  		
  	end


end
# @empleado.empleados_percepciones.build