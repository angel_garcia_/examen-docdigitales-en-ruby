class HomeController < ApplicationController
	# Valida si el usuario esta logeado antes de dejarlo entrar a los metodos del sistema
  	before_action :authenticate_user!

  	# Muestra el panel principal de la aplicacion
  def index
  	@sucursal 	=  	Sucursal.where(user_id: current_user.id)
  	#@user 		=	current_user.email

  end

  def sucursales
  	@sucursal =  Sucursal.all
  end
end
