class Empleado < ApplicationRecord
	validates :nombre, :presence => {:message => "No puedes dejar el nombre del empleado vacio" }
	validates :rfc, :presence => {:message => "No puedes dejar el RFC del empleado vacio" }
  	belongs_to :sucursal, optional: true
  	has_one	:sucursal

  	# Se crea metodo para buscar
  	def self.buscar_por_sucursal(sucursal_id)
		where(:sucursal_id => sucursal_id)
	end



end
