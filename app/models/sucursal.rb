class Sucursal < ApplicationRecord
	validates :nombre, :presence => {:message => "No puedes dejar el nombre de la sucursal vacio" }
	validates_numericality_of :numExt, :only_integer => true, :allow_nil => true, :message => "Tiene que ser un numero"
	validates_numericality_of :numInt, :only_integer => true, :allow_nil => true, :message => "Tiene que ser un numero"
	validates_numericality_of :cp, :only_integer => true, :allow_nil => true, :message => "Tiene que ser un numero"
	has_many :empleados, :foreign_key => "sucursal_id", :dependent => :destroy
	accepts_nested_attributes_for :empleados, allow_destroy: true, reject_if: :all_blank
	belongs_to :user 

	


end
