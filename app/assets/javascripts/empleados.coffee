# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

empleadosController = Paloma.controller('Empleados')


empleadosController::index = ->

  return

empleadosController::new = ->
  valida_empleado()
  

empleadosController::edit = ->





# Funciones en newController

valida_empleado = ->

	$('form').on 'submit', (e) ->
		$('#empleado_nombre,#empleado_rfc').removeClass 'errorValidation'
		empleado_nombre = $('#empleado_nombre').val()
		empleado_rfc 	  = $('#empleado_rfc').val()

		if empleado_nombre == ""
			sweetAlert("Upps","No puedes dejar vacio el nombre del empleado","warning")
			$('#empleado_nombre').addClass 'errorValidation'
			return false

		if empleado_rfc == ""
			sweetAlert("Upps","Debes incluir el RFC","warning")
			$('#empleado_rfc').addClass 'errorValidation'
			return false

		return
	

