# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# sucursalesController = Paloma.controller('sucursales')



SucursalesController = Paloma.controller('Sucursales')


#Inicia todo lo relacionado con el New

SucursalesController::new = ->

	valida_sucursal(1)
	
	limita_empleados_por_sucursal()



#Inicia todo lo relacionado con el edit

SucursalesController::edit = ->

	valida_sucursal()

# return

#Inicia todo lo relacionado con el New

SucursalesController::show = ->

	# recarga()
	maximodeEmpleados()

# return



# Inicia todo lo relacionado con el index


SucursalesController::index = ->
	#Habilita boton para crear empleados
	crearEmpleados()
	console.log  'Bienvenido'



# return

crearEmpleados = () ->
	if $('.container tr').length == 0
		$("#new").hide()
	else
		$("#new").show()
	return 
		 


valida_sucursal = (opcion)->

	$('form').on 'submit', (e) ->
		$('#sucursal_nombre,#sucursal_numInt,#sucursal_numExt,#sucursal_cp').removeClass 'errorValidation'
		nombreSucursal = $('#sucursal_nombre').val()
		numInt 	= $('#sucursal_numInt').val()
		numExt 	= $('#sucursal_numExt').val()
		cp 		= $('#sucursal_cp').val()
		
		# Validaciones al crear una nueva sucursal
		if nombreSucursal == ""
			sweetAlert("Upps","No puedes dejar la sucursal sin nombre","warning")
			$('#sucursal_nombre').addClass 'errorValidation'
			return false
			e.preventDefault()
			e.stopPropagation()

		if isNaN(numInt)
			sweetAlert("Upps","Numero interno tiene que ser un valor numerico","warning")
			$('#sucursal_numInt').addClass 'errorValidation'
			e.preventDefault()
			e.stopPropagation()

		if isNaN(numExt)
			sweetAlert("Upps","Numero externo tiene que ser un valor numerico","warning")
			$('#sucursal_numExt').addClass 'errorValidation'
			e.preventDefault()
			e.stopPropagation()

		if isNaN(cp)
			sweetAlert("Upps","Codigo postal tiene que ser un valor numerico","warning")
			$('#sucursal_cp').addClass 'errorValidation'
			e.preventDefault()
			e.stopPropagation()

		if	opcion == 1
			if valida_informacion_empleados() == 0
				e.preventDefault()
				e.stopPropagation()
		return
	

valida_informacion_empleados = ->
	$('#empleados :input').removeClass 'errorValidation'
	d = $('#fieldsActivos').val().split(",")
	i = 0
	flag = 1
	while i < d.length
		if $('#sucursal_empleados_attributes_' + d[i] + '_nombre').val() == ''
			sweetAlert("Upps","No puedes dejar el nombre vacio","warning");
			$("#sucursal_empleados_attributes_" + d[i] + "_nombre").addClass 'errorValidation'
			flag = 0;
			break
			
		if $('#sucursal_empleados_attributes_' + d[i] + '_rfc').val() == ''
			sweetAlert("Upps","No puedes dejar el RFC vacio","warning");
			$("#sucursal_empleados_attributes_" + d[i] + "_rfc").addClass 'errorValidation'
			flag = 0;
			break
		i++
	return flag
	return
		





limita_empleados_por_sucursal = ->
	empleadosCreados = 1
	lista = [0];
	$("#fieldsActivos").attr('value', lista)
	$(document).on 'fields_added.nested_form_fields', (e,p) ->
		lista.push(p.added_index)
		$("#fieldsActivos").attr('value', lista)
		limita_empleados_acccion(empleadosCreados++)

	$(document).on 'fields_removed.nested_form_fields', (e,p) ->
		lista.splice(p.removed_index,1)
		$("#fieldsActivos").attr('value', lista)
		limita_empleados_acccion(empleadosCreados--)
	return

limita_empleados_acccion = (num) ->
	if num == 4
		$('#addNueva').hide()
	else
		$('#addNueva').show()
	return





maximodeEmpleados = ->
	$(document).ready ->
		if	$('.tablaEmpleados tr').length == 6
			$('.botonAgregarEmpleado').hide()
		else
			$('.botonAgregarEmpleado').show()



