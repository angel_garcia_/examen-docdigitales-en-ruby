
UsersController = Paloma.controller('Devise/Sessions')

UsersController::new = ->
	#Habilita boton para crear empleados
	console.log  'Iniciar sesion'
	valida_inicio_de_sesion()

UsersController = Paloma.controller('Devise/Registrations')

UsersController::new = ->
	#Habilita boton para crear empleados
	console.log  'Registro de empleado'
	valida_registro_de_usuario()

UsersController::create = ->
	#Habilita boton para crear empleados
	console.log  'crear Registro de empleado'
	valida_registro_de_usuario()

###
Funciones new
###

valida_inicio_de_sesion = ->

	$('form').on 'submit', (e) ->
		$('#user_email,#user_password').removeClass 'errorValidation'
		user = $('#user_email').val()
		pass 	  = $('#user_password').val()

		if user == ""
			sweetAlert("Upps","No puedes dejar vacio el correo del empleado","warning")
			$('#user_email').addClass 'errorValidation'
			return false

		if pass == ""
			sweetAlert("Upps","Debes incluir tu password","warning")
			$('#user_password').addClass 'errorValidation'
			return false

		return
	
valida_registro_de_usuario = ->

	$('form').on 'submit', (e) ->
		$('#user_nombre,#user_rfc,#user_empresa,#user_email,#user_password,#user_password_confirmation').removeClass 'errorValidation'
		user 	= $('#user_nombre').val()
		rfc 	= $('#user_rfc').val()
		empresa = $('#user_empresa').val()
		mail	= $('#user_email').val()
		pass	= $('#user_password').val()
		pass2	= $('#user_password_confirmation').val()

		if user == ""
			sweetAlert("Upps","Ingresa el nombre del empleado","warning")
			$('#user_nombre').addClass 'errorValidation'
			return false

		if rfc == ""
			sweetAlert("Upps","Ingresa el RFC del empleado","warning")
			$('#user_rfc').addClass 'errorValidation'
			return false

		if empresa == ""
			sweetAlert("Upps","Ingresa el nombre de la empresa del empleado","warning")
			$('#user_empresa').addClass 'errorValidation'
			return false

		if mail == ""
			sweetAlert("Upps","Ingresa el correo del empleado","warning")
			$('#user_email').addClass 'errorValidation'
			return false

		if pass == ""
			sweetAlert("Upps","No puedes dejar vacio el campo del password","warning")
			$('#user_password').addClass 'errorValidation'
			return false

		if pass2 == ""
			sweetAlert("Upps","Debes incluir la confirmacion de tu password","warning")
			$('#user_password_confirmation').addClass 'errorValidation'
			return false

		if pass != pass2
			sweetAlert("Upps","Los password no coinciden","warning")
			$('#user_password_confirmation').addClass 'errorValidation'
			return false

		return
	