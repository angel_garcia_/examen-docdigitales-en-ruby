require "test_helper"

class SucursalFlowTest < ActionDispatch::IntegrationTest
  
  
  setup do
    sign_in users(:usuario_completo)
    @sucursal = sucursals(:sucursales1)
  end
  

  test "Puede ver el index" do
    varo = get "/"
    # Revisa si se carga la plantilla
    assert_select "h1", "Sucursales"
  end

  test "Debe poder crear una sucursal" do
    get "/sucursales/new"
    assert_response :success
   
    post "/sucursales",
      params: { sucursal: { nombre: "nombre Falso"} }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    
    # Revisa si se carga la plantilla
    assert_select "th", "Nombre"
  end

  test "Debe poder ver una sucursal" do
    get sucursale_path(@sucursal)
    assert_response :success
    
    # Revisa si se carga la plantilla
    assert_select "th", "Nombre"
  end

  test "Debe poder eliminar una sucursal" do
    assert_difference('Sucursal.count', -1) do
      delete sucursale_path(@sucursal)
    end

    assert_redirected_to sucursales_path
  end

  test "Debe poder actualizar una sucursal" do
    @u = User.first()
    @sucursal.user_id = @u.id
    @suc = Sucursal.first()
    @sucursal.id = @suc.id
    patch sucursale_path(@sucursal), params: { sucursal: { nombre: "updated", user_id: @u.id  } }
    assert_redirected_to sucursale_path(@sucursal)
    @sucursal.reload
    assert_equal "updated", @sucursal.nombre
    follow_redirect!
    assert_response :success

    # Revisa si se carga la plantilla
    assert_select "th", "Nombre"

  end


end