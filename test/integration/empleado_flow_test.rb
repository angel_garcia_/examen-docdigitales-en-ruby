require "test_helper"

class EmpleadoFlowTest < ActionDispatch::IntegrationTest
  
  
  setup do
    sign_in users(:usuario_completo)
    @empleado = empleados(:empleadoX)
  end
  
  test "Debe poder crear un empleado" do
    get "/empleados/new"
    assert_response :success
   
    post "/empleados",
      params: { empleado: { nombre: "nombre Falso"} }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    
    # Revisa si se carga la plantilla
    assert_select "th", "Nombre"
  end


  test "Debe poder eliminar un empleado" do
    @suc = Sucursal.first()
    @empleado.sucursal_id = @suc.id 

    assert_difference('Empleado.count', -1) do
      delete empleado_path(@empleado)
    end
    assert_redirected_to sucursale_path(@suc.id)
    follow_redirect!
    assert_response :success

    # Revisa si se carga la plantilla
    assert_select "h2", "Empleados de sucursal"
  end



  test "Debe poder actualizar un empleado" do
    @s = Sucursal.first
        
    @emp = Empleado.first()
    
    @empleado.id = @emp.id

    @empleado.sucursal_id = nil

    @empleado.sucursal_id = @s.id

    patch empleado_path(@empleado), params: { empleado: { nombre: "updated", id: @empleado.id  } }
    
    assert_redirected_to sucursale_path(@empleado.sucursal_id)
    
    @empleado.reload
    
    assert_equal "updated", @empleado.nombre
    
    follow_redirect!
    
    assert_response :success

    # Revisa si se carga la plantilla
    assert_select "th", "Nombre"

  end


end