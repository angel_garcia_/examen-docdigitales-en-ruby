require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test 'usuario invalido' do
    user  = users(:usuario_incompleto)
    assert_not user.valid?, "Guardo un usuario invalido"
  end

  test 'usuario valido' do
    user  = users(:usuario_completo)
    assert user.valid?, "No pudo guardar usuario valido"
  end


end
