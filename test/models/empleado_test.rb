require 'test_helper'

class EmpleadoTest < ActiveSupport::TestCase
  fixtures :empleados

  def setup
      @empleado = empleados(:empleado)
  end

  test "Deberia de poder crear empleado completa" do
      assert @empleado.valid?, "Deberia guardar un empleado completo"
  end
    
  test 'No debe crear empleado sin nombre' do
    @empleado.nombre = nil
    assert_not @empleado.valid?, "No deberia guardar un empleado sin nombre"
 end   

 test 'No debe crear empleado sin rfc' do
     @empleado.rfc = nil
     assert_not @empleado.valid?, "No deberia guardar un empleado sin rfc"
  end  

   test "Deberia poder eliminar empleado" do
      @emp = Empleado.find(@empleado.id).destroy
      assert @emp.destroyed?, "Deberia poder eliminar al empleado"
  end

  test "Deberia poder actualizar el empleado" do
    @emp = Empleado.find(@empleado.id)
    @emp.update({:nombre=>"updated",:id=>@emp.id})
    assert @emp.valid?, "No puede actualizar al empleado"
  end
end
