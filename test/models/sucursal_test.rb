require 'test_helper'

class SucursalTest < ActiveSupport::TestCase

    fixtures :sucursals

    def setup
        @sucursal = sucursals(:sucursal_completa)
        @sucursal_updated = sucursals(:sucursal_update)
        # @empleado = sucursals(:empleado_sucursal)
    end
    
    
    test "Deberia de poder crear sucursal completa" do
        assert @sucursal.valid?, "Deberia guardar una sucursal sin nombre"
    end
      
    test 'No debe crear sucursal incompleta' do
        @sucursal.nombre = nil
        assert_not @sucursal.valid?, "No deberia guardar una sucursal sin nombre"
     end  

     test "Deberia poder eliminar sucursal" do
        @suc = Sucursal.find(@sucursal.id).destroy
        assert @suc.destroyed?, "Deberia poder eliminar la sucursal"
    end

    test "Deberia poder actualizar sucursal" do
        @sucu = Sucursal.find(@sucursal.id)
  		@sucu.update({:nombre=>"updated",:id=>@sucu.id})
  		assert @sucu.valid?, "No puede actualizar una sucursal"
    end


    test "Deberia poder crear sucursal con empleados" do
        @e = Empleado.count
        @emp = {"0"=>{'nombre'=>"juanito",:rfc=>"HGJGH",:puesto=>"minuevopuestp"},"1"=>{'nombre'=>"juanito",:rfc=>"HGJGH",:puesto=>"minuevopuestp"}}
        @sucu = Sucursal.create({"nombre"=>"nuevaSucu","user_id"=>@sucursal.user_id, "empleados_attributes" => @emp})
        assert @sucu.valid?, "No se pudo guardar la sucursal"
        @e2 = Empleado.count
        assert_not_equal @e, @e2, "No hubo cambios en la db"
        # assert @emp.valid?
        # assert @sucursal.valid?, "Deberia guardar una sucursal sin nombre"
    end


end
