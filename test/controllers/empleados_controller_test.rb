require 'test_helper'

class EmpleadosControllerTest < ActionDispatch::IntegrationTest

  # test "listado de empleados" do
  #   assert_routing '/', controller: 'empleados', action: 'index'
  # end
  
  test "mostar plantilla new" do
    assert_routing '/empleados/new', controller: 'empleados', action: 'new'
  end

  test "crear empleado" do
    assert_routing({ path: '/empleados', method: "post" }, { controller: "empleados", action: "create"})
  end

  test "mostar empleado" do
    assert_routing '/empleados/496382230', controller: 'empleados', action: 'show', id: "496382230"
  end
  
  test "editar empleado" do
    opts = {:controller => "empleados", :action => "edit", :id => "496382230"}
    assert_routing 'empleados/496382230/edit', opts
  end

  test "actualizar empleado" do
    assert_routing({ method: 'put', path: '/empleados/496382230' }, { controller: "empleados", action: "update", id: "496382230" })
  end
  
  test "eliminar empleado" do
    assert_routing({ method: 'delete', path: '/empleados/496382230' }, { controller: "empleados", action: "destroy", id: "496382230" })

  end

end

