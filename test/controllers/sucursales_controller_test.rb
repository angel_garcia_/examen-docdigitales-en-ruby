require 'test_helper'

class SucursalesControllerTest < ActionDispatch::IntegrationTest
  
  test "listado de sucursales" do
    assert_routing '/', controller: 'sucursales', action: 'index'
  end
  
  test "mostar plantilla new" do
    assert_routing '/sucursales/new', controller: 'sucursales', action: 'new'
  end

  test "crear sucursal" do
    assert_routing({ path: '/sucursales', method: "post" }, { controller: "sucursales", action: "create"})
  end

  test "mostar sucursal" do
    assert_routing '/sucursales/496382230', controller: 'sucursales', action: 'show', id: "496382230"
  end
  
  test "editar sucursal" do
    opts = {:controller => "sucursales", :action => "edit", :id => "496382230"}
    assert_routing 'sucursales/496382230/edit', opts
  end

  test "actualizar sucursal" do
    assert_routing({ method: 'put', path: '/sucursales/496382230' }, { controller: "sucursales", action: "update", id: "496382230" })
  end
  
  test "eliminar sucursal" do
    assert_routing({ method: 'delete', path: '/sucursales/496382230' }, { controller: "sucursales", action: "destroy", id: "496382230" })

  end
  
end
